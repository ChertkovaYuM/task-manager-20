package ru.tsc.chertkova.tm.repository;

import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public M findById(final String id) {
        for (final M model :
                models) {
            if (model.getId().equals(id)) {
                return model;
            }
        }
        return null;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final Sort sort) {
        final List<M> result = new ArrayList<>(models);
        result.sort(sort.getComparator());
        return result;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

}
