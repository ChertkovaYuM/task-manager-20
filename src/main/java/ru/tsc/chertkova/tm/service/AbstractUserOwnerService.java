package ru.tsc.chertkova.tm.service;

import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.api.service.IUserOwnerService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnerService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null) throw new IdEmptyException();
        return repository.findById(userId, id);
    }

    @Override
    public int getSize(final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(userId, model);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        if (userId == null) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null) throw new UserIdEmptyException();
        if (sort == null) return repository.findAll(userId);
        return repository.findAll(userId, sort);
    }

}
