package ru.tsc.chertkova.tm.service;

import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.api.service.IService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public M remove(final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
