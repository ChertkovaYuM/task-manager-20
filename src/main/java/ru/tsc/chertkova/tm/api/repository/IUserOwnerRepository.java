package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    M add(String userId, M model);

    void clear(String userId);

    List<M> findAll(String userId);

    boolean existsById(String userId, String id);

    M findById(String userId, String id);

    int getSize(String userId);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    List<M> findAll(String userId, Comparator comparator);

    List<M> findAll(String userId, Sort sort);

}
