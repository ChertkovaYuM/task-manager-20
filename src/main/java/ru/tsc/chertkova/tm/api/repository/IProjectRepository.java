package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}
