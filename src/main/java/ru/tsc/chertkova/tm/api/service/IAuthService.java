package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

    User getUser();

    String getUserId();

    boolean isAuth();

    void checkRoles(Role[] roles);

}
