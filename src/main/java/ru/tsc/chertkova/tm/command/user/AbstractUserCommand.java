package ru.tsc.chertkova.tm.command.user;

import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    public String getArgument() {
        return null;
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
